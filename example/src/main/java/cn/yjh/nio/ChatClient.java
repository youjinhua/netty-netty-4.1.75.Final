package cn.yjh.nio;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class ChatClient {

    Logger logger = LoggerFactory.getLogger(ChatClient.class);

    private Selector selector;

    private SocketChannel sc;

    public ChatClient() {
        try {
            selector = Selector.open();
            sc = SocketChannel.open();
            sc.configureBlocking(false);
            sc.register(selector, SelectionKey.OP_CONNECT);
            sc.connect(new InetSocketAddress("127.0.0.1",6060));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){
        try {
            while (!Thread.interrupted()){
                selector.select(2000);
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()){
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if(key.isConnectable()){
                        if (sc.finishConnect()) {
                            key.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Scanner scanner = new Scanner(System.in);
                                        while (true) {
                                            String str = scanner.nextLine();
                                            ByteBuffer buffer = ByteBuffer.allocate(1024);
                                            buffer.put(str.getBytes(StandardCharsets.UTF_8));
                                            buffer.flip();
                                            sc.write(buffer);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                        }
                    }
                    if(key.isReadable()){
                        SocketChannel sc = (SocketChannel) key.channel();
                        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                        int len = sc.read(byteBuffer);
                        if(len > 0){
                            logger.info("{}",new String(byteBuffer.array(), 0, byteBuffer.position(), StandardCharsets.UTF_8));
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ChatClient().start();
    }
}
