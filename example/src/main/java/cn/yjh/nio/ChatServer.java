package cn.yjh.nio;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:00:33
 * @VERSION: 1.0
 */
public class ChatServer {

    Logger logger = LoggerFactory.getLogger(ChatServer.class);

    private Selector selector;

    private ServerSocketChannel ssc;

    private static final int PORT = 6060;

    private Map<String,SocketChannel> keyMap;

    public ChatServer() {
        try {
            selector = Selector.open();
            ssc = ServerSocketChannel.open();
            ssc.configureBlocking(false);
            ssc.register(selector, SelectionKey.OP_ACCEPT);
            ssc.bind(new InetSocketAddress(PORT));
            keyMap = new HashMap<>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){

        logger.info(" Chat Server Start at [{}]",PORT);
        while(!Thread.interrupted()){
            try{
                selector.select(2000);
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()){
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if(key.isAcceptable()){
                        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
                        SocketChannel socketChannel = ssc.accept();
                        socketChannel.configureBlocking(false);
                        socketChannel.register(selector,SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                        String name = socketChannel.getRemoteAddress().toString();
                        logger.info("{} 上线",socketChannel.getRemoteAddress());
                        if(keyMap.size()>0){
                            Iterator<String> ite = keyMap.keySet().iterator();
                            while (ite.hasNext()){
                                ByteBuffer buffer = ByteBuffer.allocate(1024);
                                String next = ite.next();
                                SocketChannel sc = keyMap.get(next);
                                if(!next.equals(name)){
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(name+" 上线了......");
                                    buffer.put(sb.toString().getBytes(StandardCharsets.UTF_8));
                                    buffer.flip();
                                    sc.write(buffer);
                                }
                            }
                        }
                        keyMap.put(name,socketChannel);
                    }
                    if(key.isReadable()){
                        SocketChannel sc = (SocketChannel) key.channel();
                        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                        int len = 0;
                        try{
                            len = sc.read(byteBuffer);
                        }catch (IOException e){
                            key.cancel();
                            String name = sc.getRemoteAddress().toString();
                            logger.info("{} 下线",name);
                            if(keyMap.size()>0){
                                Iterator<String> ite = keyMap.keySet().iterator();
                                while (ite.hasNext()){
                                    StringBuilder sb = new StringBuilder();
                                    String next = ite.next();
                                    sb.append(sc.getRemoteAddress().toString()+" 下线了......");
                                    if(!next.equals(name)){
                                        SocketChannel sc0 = keyMap.get(next);
                                        ByteBuffer allocate = ByteBuffer.allocate(1024);
                                        allocate.put(sb.toString().getBytes(StandardCharsets.UTF_8));
                                        allocate.flip();
                                        sc0.write(allocate);
                                    }else{
                                        ite.remove();
                                    }

                                }
                            }
                            break;
                        }
                        String context = new String(byteBuffer.array(), 0, byteBuffer.position(), StandardCharsets.UTF_8);
                        if(len > 0){
                            logger.info("{}: {}",sc.getRemoteAddress(),context);
                        }

                        if(keyMap.size()>0){
                            Iterator<String> ite = keyMap.keySet().iterator();
                            while (ite.hasNext()){
                                StringBuilder sb = new StringBuilder();
                                String next = ite.next();
                                sb.append("["+sc.getRemoteAddress().toString()+"] 说：");
                                sb.append(context);
                                SocketChannel sc0 = keyMap.get(next);
                                ByteBuffer allocate = ByteBuffer.allocate(1024);
                                allocate.put(sb.toString().getBytes(StandardCharsets.UTF_8));
                                allocate.flip();
                                sc0.write(allocate);
                            }
                        }
                    }

                }
            }catch (IOException e){
                if(keyMap.size()>0){
                    Iterator<String> ite = keyMap.keySet().iterator();
                    while (ite.hasNext()){
                        StringBuilder sb = new StringBuilder();
                        String next = ite.next();
                       // sb.append("["+sc.getRemoteAddress().toString()+"] 说：");
                        //sb.append(context);
                        SocketChannel sc0 = keyMap.get(next);
                        ByteBuffer allocate = ByteBuffer.allocate(1024);
                        allocate.put(sb.toString().getBytes(StandardCharsets.UTF_8));
                        allocate.flip();
                        //sc0.write(allocate);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        new ChatServer().start();

    }
}
