package cn.yjh.netty.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class ByteToLongDecoderHandler extends ByteToMessageDecoder {

    Logger logger = LoggerFactory.getLogger(ByteToLongDecoderHandler.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        Channel channel = ctx.channel();
        logger.info("入站......");
        // 按照一个 long 类型的长度读取数据
        if(in.readableBytes() >= 8){
            out.add(in.readLong());
        }
    }
}
