package cn.yjh.netty.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class LongToByteEncoderHandler extends MessageToByteEncoder<Long> {

    Logger logger = LoggerFactory.getLogger(LongToByteEncoderHandler.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, Long msg, ByteBuf out) throws Exception {
        Channel channel = ctx.channel();
        logger.info("出站......");
        out.writeLong(msg);
    }
}
