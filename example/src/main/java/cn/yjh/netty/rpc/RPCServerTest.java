package cn.yjh.netty.rpc;

import cn.yjh.netty.rpc.net.RPCBootstrap;

/**
 * @author yW0041229
 * @since 2022/4/11 16:59
 */
public class RPCServerTest {

    public static void main(String[] args) {
        RPCBootstrap.start0("127.0.0.1",2090);
    }
}
