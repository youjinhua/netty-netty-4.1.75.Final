package cn.yjh.netty.rpc;

import cn.yjh.netty.rpc.api.HelloService;
import cn.yjh.netty.rpc.net.RPCProxy;

/**
 * @author yW0041229
 * @since 2022/4/11 17:44
 */
public class RPCClientTest {

    public static void main(String[] args) {
        RPCProxy proxy = new RPCProxy();
        HelloService service = proxy.getBean(HelloService.class, "RPC/1.1");
        service.hello("YouJinhua");
    }
}
