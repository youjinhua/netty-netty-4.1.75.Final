package cn.yjh.netty.rpc.net;

import java.lang.reflect.Proxy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @author yW0041229
 * @since 2022/4/11 17:29
 */
public class RPCProxy {

    // 创建线程池
    private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static InvokerHandler invokerHandler;

    // 初始化客户端
    private static void initClient(){
        invokerHandler = new InvokerHandler();
        NioEventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.TCP_NODELAY,true);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new StringEncoder());
                pipeline.addLast(new StringDecoder());
                pipeline.addLast(invokerHandler);
            }
        });
        try{
            bootstrap.connect("127.0.0.1", 2090);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public <T> T getBean(final Class<T> clazz,final String protocol){
        return (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),new Class<?>[] {clazz},((proxy, method, args) -> {
            if(invokerHandler == null){
                initClient();
            }
            invokerHandler.setParam(args[0]+"");
            return executor.submit(invokerHandler).get();
        }));
    }
}
