package cn.yjh.netty.rpc.net;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.LockSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author yW0041229
 * @since 2022/4/11 17:04
 */
public class InvokerHandler extends ChannelInboundHandlerAdapter implements Callable {

    Logger logger = LoggerFactory.getLogger(InvokerHandler.class);

    private ChannelHandlerContext context;

    private String reslut;

    private String param;

    private Thread callThread;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("设置 context 上下文......");
        this.context = ctx;
        //notify();
        LockSupport.unpark(callThread);

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        reslut = msg.toString();
        // 唤醒等待的线程
        LockSupport.unpark(callThread);
        //notify();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        logger.info("添加 InvokerHandler 完成之后执行 handlerAdded......");
    }

    @Override
    public Object call() throws Exception {
        logger.info("调用 call 方法......");
        if(context == null){
            callThread = Thread.currentThread();
            LockSupport.park();
        }
        context.writeAndFlush("RPC/1.1_"+param);
        // 等待被唤醒
        LockSupport.park();
        //wait();
        logger.info("服务端返回消息：{}",reslut);
        // 返回结果
        return reslut;
    }

    void setParam(String param){
        this.param = param;
    }
}
