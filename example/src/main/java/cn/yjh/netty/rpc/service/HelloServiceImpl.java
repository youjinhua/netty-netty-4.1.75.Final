package cn.yjh.netty.rpc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.yjh.netty.rpc.api.HelloService;

/**
 * @author yW0041229
 * @since 2022/4/11 16:40
 */
public class HelloServiceImpl implements HelloService {

    Logger logger = LoggerFactory.getLogger(HelloServiceImpl.class);

    @Override
    public String hello(String msg) {
        logger.info("接收到客户端消息：{}",msg);
        if(msg != null){
            return "你好客户端，我已经收到你的消息：[ "+msg+" ]";
        }
        return "你好客户端，我已经收到你的消息!";
    }
}
