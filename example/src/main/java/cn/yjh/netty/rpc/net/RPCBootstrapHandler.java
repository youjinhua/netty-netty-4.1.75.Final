package cn.yjh.netty.rpc.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.yjh.netty.rpc.service.HelloServiceImpl;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author yW0041229
 * @since 2022/4/11 16:52
 */
public class RPCBootstrapHandler extends ChannelInboundHandlerAdapter {

    Logger logger = LoggerFactory.getLogger(RPCBootstrapHandler.class);


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.info("recived：{}",msg);
        if(msg.toString().startsWith("RPC/1.1")){
            HelloServiceImpl helloService = new HelloServiceImpl();
            String result = helloService.hello(msg.toString());
            ctx.writeAndFlush(result);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
