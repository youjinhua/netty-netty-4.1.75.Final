package cn.yjh.netty.rpc.api;

/**
 * @author yW0041229
 * @since 2022/4/11 16:39
 */
public interface HelloService {

    String hello(String msg);
}
