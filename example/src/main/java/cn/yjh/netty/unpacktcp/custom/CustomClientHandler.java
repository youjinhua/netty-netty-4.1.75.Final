package cn.yjh.netty.unpacktcp.custom;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class CustomClientHandler extends SimpleChannelInboundHandler<MessageProtocol> {

    Logger logger = LoggerFactory.getLogger(CustomClientHandler.class);

    private int count;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 使用客户端发送 10 条数据
        for (int i = 0; i < 3; i++){
            String msg = "谁的新欢不是别人艹到想吐的旧爱，别当真就好！";
            byte[] content = msg.getBytes(StandardCharsets.UTF_8);
            int len = content.length;
            MessageProtocol messageProtocol = new MessageProtocol(len, content);
            ctx.writeAndFlush(messageProtocol);
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageProtocol messageProtocol) throws Exception {
        int len = messageProtocol.getLen();
        byte[] content = messageProtocol.getContent();
        String message = new String(content, Charset.forName("UTF-8"));
        logger.info("服务端接收消息次数：{}，消息体：{}，长度为： [ {} ]",(++this.count),message,len);

    }
}
