package cn.yjh.netty.unpacktcp.custom;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class CustomServerHandler extends SimpleChannelInboundHandler<MessageProtocol> {

    Logger logger = LoggerFactory.getLogger(CustomServerHandler.class);

    private  int count;

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageProtocol msg) throws Exception {
        int len = msg.getLen();
        byte[] content = msg.getContent();
        String message = new String(content, Charset.forName("UTF-8"));
        logger.info("服务端接收消息次数：{}，消息体：{}，长度为： [ {} ]",(++this.count),message,len);

        // 服务器回送数据回客户端，回送一个随机 ID 值
        String responseContent = UUID.randomUUID().toString().replace("-","");
        byte[] responseBytes = responseContent.getBytes(StandardCharsets.UTF_8);
        int responseLen = responseBytes.length;
        MessageProtocol messageProtocol = new MessageProtocol(responseLen, responseBytes);
        ctx.writeAndFlush(messageProtocol);
    }
}
