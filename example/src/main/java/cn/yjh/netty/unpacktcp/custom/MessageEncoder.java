package cn.yjh.netty.unpacktcp.custom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class MessageEncoder extends MessageToByteEncoder<MessageProtocol> {

    Logger logger = LoggerFactory.getLogger(MessageEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, MessageProtocol msg, ByteBuf out) throws Exception {
        logger.info("message encoder 被调用......");
        out.writeInt(msg.getLen());
        out.writeBytes(msg.getContent());
    }
}
