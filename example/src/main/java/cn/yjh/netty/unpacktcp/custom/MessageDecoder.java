package cn.yjh.netty.unpacktcp.custom;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class MessageDecoder extends ByteToMessageDecoder {

    Logger logger = LoggerFactory.getLogger(MessageDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        logger.info("message decoder 被调用......");
        // 将读取到的二进制字节码转成 MessageProtocol 对象
        int len = in.readInt();
        byte[]  content = new byte[len];
        in.readBytes(content);
        MessageProtocol messageProtocol = new MessageProtocol(len, content);
        out.add(messageProtocol);
    }
}
