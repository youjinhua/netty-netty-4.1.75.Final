package cn.yjh.netty.unpacktcp.custom;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class MessageProtocol {

    private int len;

    private byte[] content;

    public MessageProtocol(int len, byte[] content) {
        this.len = len;
        this.content = content;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
