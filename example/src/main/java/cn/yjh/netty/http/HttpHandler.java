package cn.yjh.netty.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class HttpHandler extends SimpleChannelInboundHandler<HttpObject> {

    Logger logger = LoggerFactory.getLogger(HttpHandler.class);


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws Exception {
        if(msg instanceof HttpRequest){
            HttpRequest request = (HttpRequest) msg;
            String uri = request.uri();
            logger.info("msg 类型：{}",msg.getClass());
            logger.info("客户端地址：{}",ctx.channel().remoteAddress());
            logger.info("请求地址：{}",uri);
            ByteBuf content = Unpooled.copiedBuffer("你好，我是 http 服务器 !", CharsetUtil.UTF_8);
            // 构建 响应 response 对象
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_0, HttpResponseStatus.OK, content);
            // 加上 charset 避免中文乱码
            response.headers().set(HttpHeaderNames.CONTENT_TYPE,"text/plain;charset=UTF-8");
            response.headers().set(HttpHeaderNames.CONTENT_LENGTH,content.readableBytes());
            // 写出数据
            ctx.writeAndFlush(response);
        }
    }

}
