package cn.yjh.netty.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class HttpChannelInitializer extends ChannelInitializer<NioSocketChannel> {


    @Override
    protected void initChannel(NioSocketChannel ch) throws Exception {

        ChannelPipeline pipeline = ch.pipeline();
        // 添加 netty 对 http 支持的自带编解码 handler
        pipeline.addLast("httpCodec",new HttpServerCodec());
        // 添加自定义的 handler
        pipeline.addLast("httpHandler",new HttpHandler());

    }
}
