package cn.yjh.netty.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class NettyClientHandler extends ChannelInboundHandlerAdapter {

    Logger logger = LoggerFactory.getLogger(NettyClientHandler.class);

    EventExecutorGroup servicePool = new DefaultEventExecutorGroup(5);

    @Override
    protected void ensureNotSharable() {
        logger.info("{} 方法在{}的时候，被调用......","ensureNotSharable()","接收到可读取数据");
    }

    @Override
    public boolean isSharable() {
        logger.info("{} 方法在{}的时候，被调用......","isSharable()","当前 handler 被添加到双向链表之前");
        return super.isSharable();
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","handlerAdded()"," handler 被添加到 handler 双向链表之后");
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","handlerRemoved()","当前 handler 被移出双向链表");
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","channelRegistered()","handler 被添加到双向链表之后，channel 被注册好了之后");
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","channelUnregistered()","取消socket注册");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","channelInactive()","调用 ctx.close() 方法关闭客户端连接");
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","channelReadComplete()","读取数据完成");
        int i = 1/0;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","userEventTriggered()","某些事件触发时会调用");
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","channelWritabilityChanged()","接收到可读取数据");
    }

    // 通道就绪之后，会调用该方法
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","channelActive()"," channel 通道建立好，连接可用之后");
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello server !", CharsetUtil.UTF_8));
    }

    // 接收到数据，会调用该方法
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        servicePool.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("{} 方法在{}的时候，被调用......","channelRead()","接收到可读取数据");
            }
        });
        ByteBuf buf = (ByteBuf) msg;
        logger.info("收到服务端：{} 的消息：{}",ctx.channel().remoteAddress(),buf.toString(CharsetUtil.UTF_8));
    }

    // 发生异常，会调用该方法
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.info("{} 方法在{}的时候，被调用......","exceptionCaught()","出现异常");
        ctx.close();
    }
}
