package cn.yjh.netty.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.DefaultEventLoop;
import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.EventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class NettyClient  {

    static Logger logger = LoggerFactory.getLogger(NettyClientHandler.class);

    public static void main(String[] args) {
        /*EventLoopGroup group = new NioEventLoopGroup();
        EventExecutorGroup servicePool = new DefaultEventExecutorGroup(5);
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.handler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel ch) throws Exception {
                 // 将 handler 添加到异步线程中去执行
                 ch.pipeline().addLast(servicePool,new NettyClientHandler());
            }
        });
        bootstrap.connect("127.0.0.1",8080);*/
        testNettyThreadPool();

    }

    public static void testNettyThreadPool(){

        DefaultEventLoop defaultEventLoop = new DefaultEventLoop();

        // 任务会提交大 taskQueue 中
        defaultEventLoop.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("当前线程：{}",Thread.currentThread());
            }
        });
        defaultEventLoop.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("当前线程：{}",Thread.currentThread());
            }
        });
        defaultEventLoop.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("当前线程：{}",Thread.currentThread());
            }
        });

       /* EventExecutorGroup defaultEventExecutorGroup = new DefaultEventExecutorGroup(3);
        EventLoopGroup defaultEventLoopGroup = new DefaultEventLoopGroup(2);

        defaultEventExecutorGroup.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("当前线程：{}",Thread.currentThread());
            }
        });
        defaultEventLoopGroup.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("当前线程：{}",Thread.currentThread());
            }
        });*/
    }

}
