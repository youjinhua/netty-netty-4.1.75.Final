package cn.yjh.netty.heart;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.yjh.netty.chat.ChatServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class HeartBeatServer {

    static Logger logger = LoggerFactory.getLogger(HeartBeatServer.class);

    public static void main(String[] args) throws Exception {
        EventLoopGroup boosGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(boosGroup,workerGroup);
        serverBootstrap.channel(NioServerSocketChannel.class);
        serverBootstrap.option(ChannelOption.SO_BACKLOG,128);
        serverBootstrap.childOption(ChannelOption.SO_KEEPALIVE,true);
        // 给 boosGroup 添加一个日志处理器
        serverBootstrap.handler(new LoggingHandler(LogLevel.INFO));
        serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();

                /**
                 *  添加一个 netty 提供的一个处理空闲连接的处理器 IdleStateHandler
                 *      readerIdleTime：表示多长时间没有读，就会发送一个心跳检测包检测连接是否正常
                 *      writerIdleTime：表示多长时间没有写，就会发送一个心跳检测包检测连接是否正常
                 *      allIdleTime：表示多长时间没有读写，就会发送一个心跳检测包检测连接是否正常
                 *      unit：时间单位
                 *  说明：当 IdleStateEvent 触发后，就会传递给管道的下一个 handler 去处理，通过调用
                 *       下一个 handler 的 userEventTrigger 方法，在这个方法中去处理 IdleStateEvent
                 *       事件
                 *
                 */
                pipeline.addLast(new IdleStateHandler(3,5,7, TimeUnit.SECONDS));

                // 加入一个对空闲检测进一步处理的自定义 handler
                pipeline.addLast(new HeartBeatServerHandler());
            }
        });
        logger.info("SERVER IS START......");
        ChannelFuture channelFuture = serverBootstrap.bind(6066).sync();
        channelFuture.channel().closeFuture().sync();
    }
}
