package cn.yjh.netty.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

/**
 * @DESC:
 * @AUTHOR: YouJinhua
 * @DATE: 2022-04-04 21:31:07
 * @VERSION: 1.0
 */
public class ChatServerHandler extends SimpleChannelInboundHandler<String> {

    Logger logger = LoggerFactory.getLogger(ChatServerHandler.class);


    // 定义一个 channel 组，管理所有的 channel
    // GlobalEventExecutor.INSTANCE 是全局的事件执行器，是一个单例的
    private static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);


    // 一旦连接建立，则会被调用
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        channelGroup.add(channel);
        channelGroup.writeAndFlush("[用户"+channel.remoteAddress()+"] 加入群聊\n");
        super.handlerAdded(ctx);
    }

    // 通道可用了，则会调用
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 上线了~",ctx.channel().remoteAddress());
        super.channelActive(ctx);
    }

    // 断开连接，则会调用
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        channelGroup.writeAndFlush("[用户"+channel.remoteAddress()+"] 离开了~\n");
        super.handlerRemoved(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.info("{} 离线了~",ctx.channel().remoteAddress());
        super.channelInactive(ctx);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        Channel channel = ctx.channel();
        // 遍历 channelGroup，根据不同情况，回送不同消息
        channelGroup.forEach(ch->{
            if(channel != ch){
                ch.writeAndFlush("[用户"+channel.remoteAddress()+"]消息："+msg+"\n");
            }else{
                ch.writeAndFlush("[自己]消息："+msg+"\n");
            }
        });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        ctx.close();
    }
}
